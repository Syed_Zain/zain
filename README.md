import numpy as np
from sklearn import preprocessing
import quandl
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt
quandl.ApiConfig.api_key = 'DwzYR6GVnm3EDV7f4Kgj'
df=quandl.get("WIKI/GOOGL")

df['pred']=df['Adj. Close'].shift(-1)

X=np.array(df.drop(['pred'],1))
y=np.array(df['pred'])
df.fillna(value=-99999, inplace=True)
X = preprocessing.scale(X)

y=np.reshape(y,(-1 , 1))
X_train=X[:-20]
y_train=y[:-20]
X_test=X[-20:]
y_test=y[-20:]

x_future=X

x_fut=x_future[-1:]


regr = linear_model.LinearRegression()
regr.fit(X_train,y_train)
y_fut=regr.predict(x_fut)

print(y_fut)